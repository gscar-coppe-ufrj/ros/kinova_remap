# Usage

```bash
roslaunch kinova_remap kinova_remap.launch
```

Runs kinova_remap node.

Subscribes:
- *namespace*/joint_command (sensor_msgs/JointState)
- *namespace*/twist_command (geometry_msgs/TwistStamped)
- *namespace*/control_mode (std_msgs/Int8)
- *namespace*/action (std_msgs/Int16):  identifier of the requested action (e.g. Home Action: 2)
- *namespace*/gripper_command (std_msgs/Float64): position command in [0.0, 1.0], 0.0 fully open - 1.0 fully closed

Publishes:  (assumes "kinova" is the kinova namespace in kortex_driver)
- /kinova/in/joint_velocity (kortex_driver/Base_JointSpeeds)
- /kinova/in/cartesian_velocity (kortex_driver/TwistCommand)
- *namespace*/action_feedback (std_msgs/Bool)

## Control Modes

Control mode is set to -1 in the beginning, which it is not controlled.
In order to switch to cartesian velocity commands, control mode should be changed to 1. It can be done by publishing the following message:

```bash
rostopic pub -1 /kinova_remap/control_mode std_msgs/Int8 "data: 1"
```

Control Mode | Description |
--- | --- |
-1 | Not Controlled |
0 | Full Joint Velocity |
1 | Arm Joint Velocity |
2 | Wrist Joint Velocity |
3 | Cartesian Velocity in Base Frame |
4 | Cartesian Linear Velocity in Base Frame |
5 | Cartesian Angular Velocity in Base Frame |
6 | Cartesian Linear Velocity in Tool Frame |
7 | Cartesian Angular Velocity in Tool Frame |

If a message is not sent to joint_command or twist_command topics for more than 1sec, they will send velocity = 0.

Kortex_driver messages have a *duration* field, which is meant to allow to set a limit (in milliseconds) to the command, but it is not implemented yet. So this field is being used as *seq* field of a std_msgs/Header.

## Parameters
These parameters can be customized in launch file.

Parameter | Type | Default Value |
--- | --- | --- |
namespace | string | "kinova_remap" |
reference_frame | int | 3 |
safety_timeout | double | 1.0
joint_command_gain | double (scalar or 7-vector) | 1.0 |
twist_command_gain | double (scalar or 6-vector) | 1.0 |

*reference_frame* parameter is the reference frame for Twist Command.
Documentation can be found [here](https://github.com/Kinovarobotics/kortex/blob/master/api_cpp/doc/markdown/enums/Common/CartesianReferenceFrame.md).

*safety_timeout* parameter: if a new message is not sent to joint_command or twist_command topics for more than the safety timeout duration (for example, 1sec), they will send velocity = 0.

## Actions

An action can be  activated by publishing the following message:

```bash
rostopic pub -1 /kinova_remap/action std_msgs/Int16 "data: 1"
```
Action | Type | Identifier |
--- | --- | --- |
Retract position | 7 | 1 |
Home position | 7 | 2 |
Packing position | 7 | 3 |
Zero position | 7 | 4 |

To obtain the Identifier of custom actions: go to Kinova Wep App --> Operations --> Actions 
and Export All (XML). Check in jointAngles.xml  the identifier number for the custom actions.

Note: the sequence action are not working. The identifier (can be obtained in sequence.xml) is not
recognized by kortex_driver.

## Gripper Commands

A gripper commands can be set (between 0 and 1) publishing the following message:

```bash
rostopic pub -1 /kinova_remap/gripper_command std_msgs/Float64 "data: 0.0"
```
