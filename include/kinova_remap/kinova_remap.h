#include <ros/ros.h>

#include <std_msgs/Int8.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/TwistStamped.h>
#include <sensor_msgs/JointState.h>
#include <kortex_driver/TwistCommand.h>
#include <kortex_driver/Base_JointSpeeds.h>
#include <kortex_driver/SendGripperCommand.h>
#include <kortex_driver/GripperMode.h>
#include <kortex_driver/ReadAction.h>
#include <kortex_driver/ExecuteAction.h>

// #include <omni_msgs/OmniButtonEvent.h>
// #include <eigen3/Eigen/Core>

class KinovaRemap
{
protected:
    // Node Handle
    ros::NodeHandlePtr nh;

    // Parameters
    int cartesian_reference_frame;
    double safety_timeout;
    std::vector<double> joint_command_gain;
    std::vector<double> twist_command_gain;
    void setParameters();

    // Publishers and Subscribers
    ros::Subscriber sub_twist_command;
    ros::Subscriber sub_joint_command;
    ros::Subscriber sub_control_mode;
    ros::Subscriber sub_action;
    ros::Subscriber sub_gripper_command;

    // ros::Subscriber sub_button_state;
    ros::Publisher pub_cartesian_velocity;
    ros::Publisher pub_joint_velocity;
    ros::Publisher pub_action_feedback;

    // Gripper service
    //ros::ServiceClient service_client_send_gripper_command;
    bool send_gripper_command(double value);

    // Actions
    bool action2kinova(int action_identifier);

    // Callbacks
    void twistCallback(const geometry_msgs::TwistStamped::ConstPtr& msg);
    void jointCommandCallback(const sensor_msgs::JointState::ConstPtr& msg);
    void controlModeCallback(const std_msgs::Int8::ConstPtr& msg);
    // void buttonStateCallback(const omni_msgs::OmniButtonEvent::ConstPtr& msg);
    void actionCallback(const std_msgs::Int16::ConstPtr& msg);
    void gripperCommandCallback(const std_msgs::Float64::ConstPtr& msg);

    // Messages
    geometry_msgs::TwistStamped master_twist_command;
    sensor_msgs::JointState master_joint_command;
    // omni_msgs::OmniButtonEvent omni_button_state;
    kortex_driver::TwistCommand kinova_twist_command;
    kortex_driver::Base_JointSpeeds kinova_joint_speeds;
    
    // Message Time
    ros::Time msg_time_twist;
    ros::Time msg_time_joint_command;

public:

    KinovaRemap();

    int control_mode;
    enum ControlMode {JointVelocity = 0, JointVelocityArm, JointVelocityWrist, CartesianVelocity, CartesianLinearVelocity, CartesianAngularVelocity, CartesianLinearVelocityTool, CartesianAngularVelocityTool, GripperControl};
    void publishCartesianVelocity();
    void publishJointVelocity();
    void publishTopic();
    
};
