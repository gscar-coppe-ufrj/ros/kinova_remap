#include "kinova_remap/kinova_remap.h"

#include <iostream>


KinovaRemap::KinovaRemap() {

    nh = ros::NodeHandlePtr(new ros::NodeHandle(""));

    setParameters();

    sub_twist_command = nh->subscribe<geometry_msgs::TwistStamped>("twist_command", 1, &KinovaRemap::twistCallback, this);
    sub_joint_command = nh->subscribe<sensor_msgs::JointState>("joint_command", 1, &KinovaRemap::jointCommandCallback, this);
    // sub_button_state = nh->subscribe<omni_msgs::OmniButtonEvent>("button_state", 1, &KinovaRemap::buttonStateCallback, this);
    sub_control_mode = nh->subscribe<std_msgs::Int8>("control_mode", 1, &KinovaRemap::controlModeCallback, this);
    sub_action = nh->subscribe<std_msgs::Int16>("action", 1, &KinovaRemap::actionCallback, this);
    sub_gripper_command = nh->subscribe<std_msgs::Float64>("gripper_command", 1, &KinovaRemap::gripperCommandCallback, this);

    pub_cartesian_velocity = nh->advertise<kortex_driver::TwistCommand>("/kinova/in/cartesian_velocity", 1);
    pub_joint_velocity = nh->advertise<kortex_driver::Base_JointSpeeds>("/kinova/in/joint_velocity", 1);
    pub_action_feedback = nh->advertise<std_msgs::Bool>("action_feedback", 1);
    
    kinova_joint_speeds.joint_speeds.resize(7);
    master_joint_command.velocity.resize(7);

    control_mode = -1;  //JointVelocityArm;
    ROS_INFO("KINOVA REMAP: Invalid control mode!");

    kinova_twist_command.reference_frame = cartesian_reference_frame;
}

void KinovaRemap::setParameters() {
    
    ros::param::param<int>("~reference_frame", cartesian_reference_frame, 3);   // Not used, now control_mode defines the frame
    ros::param::param<double>("~safety_timeout", safety_timeout, 1.0);

    ros::param::get("~twist_command_gain", twist_command_gain);
    if (twist_command_gain.size() != 1 && twist_command_gain.size() != 6) {
        ROS_WARN("Twist command gain should be represented as a scalar or a 6 elements vector. Setting gain to 1");
        twist_command_gain.resize(6);
        std::fill(twist_command_gain.begin(),twist_command_gain.end(), 1.0);
    }
    ros::param::get("~joint_command_gain", joint_command_gain);
    if (joint_command_gain.size() != 1 && joint_command_gain.size() != 7) {
        ROS_WARN("Joint command gain should be represented as a scalar or a 7 elements vector. Setting gain to 1");
        joint_command_gain.resize(7);
        std::fill(joint_command_gain.begin(),joint_command_gain.end(), 1.0);
    }
    if (twist_command_gain.size() == 1) {
        double gain = twist_command_gain.at(0);
        twist_command_gain.resize(6);
        std::fill(twist_command_gain.begin(),twist_command_gain.end(), gain);
    }
    if (joint_command_gain.size() == 1) {
        double gain = joint_command_gain.at(0);
        joint_command_gain.resize(7);
        std::fill(joint_command_gain.begin(),joint_command_gain.end(), gain);
    }
}

void KinovaRemap::twistCallback(const geometry_msgs::TwistStamped::ConstPtr& msg) {

    msg_time_twist = ros::Time::now();

    master_twist_command.twist.linear.x = msg->twist.linear.x;
    master_twist_command.twist.linear.y = msg->twist.linear.y;
    master_twist_command.twist.linear.z = msg->twist.linear.z;

    master_twist_command.twist.angular.x = msg->twist.angular.x;
    master_twist_command.twist.angular.y = msg->twist.angular.y;
    master_twist_command.twist.angular.z = msg->twist.angular.z;
}

void KinovaRemap::jointCommandCallback(const sensor_msgs::JointState::ConstPtr& msg) {

    msg_time_joint_command = ros::Time::now();

    for (int i = 0; i < 7; i++)
    {
        master_joint_command.velocity.at(i) = msg->velocity.at(i);
    }
}

void KinovaRemap::controlModeCallback(const std_msgs::Int8::ConstPtr& msg) {
    control_mode = msg->data;
    switch (control_mode) {
    case JointVelocity:
        ROS_INFO("KINOVA REMAP: Full Joint Velocity Control");
        break;
    case JointVelocityArm:
        ROS_INFO("KINOVA REMAP: Arm Joint Velocity Control");
        break;
    case JointVelocityWrist:
        ROS_INFO("KINOVA REMAP: Wrist Joint Velocity Control");
        break;
    case CartesianVelocity:
        ROS_INFO("KINOVA REMAP: Cartesian Velocity Control");
        kinova_twist_command.reference_frame = 3; //CARTESIAN_REFERENCE_FRAME_BASE;
        break;
    case CartesianLinearVelocity:
        ROS_INFO("KINOVA REMAP: Cartesian Linear Velocity Control");
        kinova_twist_command.reference_frame = 3; //CARTESIAN_REFERENCE_FRAME_BASE;
        break;
    case CartesianAngularVelocity:
        ROS_INFO("KINOVA REMAP: Cartesian Angular Velocity Control");
        kinova_twist_command.reference_frame = 3; //CARTESIAN_REFERENCE_FRAME_BASE;
        break;
    case CartesianLinearVelocityTool:
        ROS_INFO("KINOVA REMAP: Cartesian Linear Velocity Control in Tool Frame");
        kinova_twist_command.reference_frame = 2; //CARTESIAN_REFERENCE_FRAME_TOOL;
        break;
    case CartesianAngularVelocityTool:
        ROS_INFO("KINOVA REMAP: Cartesian Angular Velocity Control in Tool Frame");
        kinova_twist_command.reference_frame = 2; //CARTESIAN_REFERENCE_FRAME_TOOL;
        break;
    case GripperControl:
        ROS_INFO("KINOVA REMAP: Gripper control");
        break;
    default:
        ROS_WARN("KINOVA REMAP: Invalid control mode!");
        break;
    }
}

void KinovaRemap::actionCallback(const std_msgs::Int16::ConstPtr& msg) {
    int action_identifier = msg->data;

    // Action Function
    bool action_feedback = action2kinova(action_identifier);

    // Message
    std_msgs::Bool action_feedback_msg;
    action_feedback_msg.data = action_feedback;

    // Publish Feedback
    pub_action_feedback.publish(action_feedback_msg);
}

void KinovaRemap::gripperCommandCallback(const std_msgs::Float64::ConstPtr& msg) {
    double command = msg->data;

    // Action Function

    switch (control_mode){
    case GripperControl:
        bool feedback = send_gripper_command(command);
        break;
    }
}

void KinovaRemap::publishCartesianVelocity() {

    // twist_command.duration not yet implemeted, so it is used as a "header.seq".
    // Once it is implemented, duration in milliseconds.
    static int duration = 0;
    kinova_twist_command.duration = duration++;

    double time_delta = (ros::Time::now() - msg_time_twist).toSec();

    if (time_delta < safety_timeout){
        switch (control_mode) {
        case CartesianVelocity:
            kinova_twist_command.twist.linear_x = twist_command_gain.at(0)*(master_twist_command.twist.linear.x);
            kinova_twist_command.twist.linear_y = twist_command_gain.at(1)*(master_twist_command.twist.linear.y);
            kinova_twist_command.twist.linear_z = twist_command_gain.at(2)*(master_twist_command.twist.linear.z);
        
            kinova_twist_command.twist.angular_x = twist_command_gain.at(3)*(master_twist_command.twist.angular.x);
            kinova_twist_command.twist.angular_y = twist_command_gain.at(4)*(master_twist_command.twist.angular.y);
            kinova_twist_command.twist.angular_z = twist_command_gain.at(5)*(master_twist_command.twist.angular.z);
            break;
        case CartesianLinearVelocity:
            kinova_twist_command.twist.linear_x = twist_command_gain.at(0)*(master_twist_command.twist.linear.x);
            kinova_twist_command.twist.linear_y = twist_command_gain.at(1)*(master_twist_command.twist.linear.y);
            kinova_twist_command.twist.linear_z = twist_command_gain.at(2)*(master_twist_command.twist.linear.z);
        
            kinova_twist_command.twist.angular_x = 0.0;
            kinova_twist_command.twist.angular_y = 0.0;
            kinova_twist_command.twist.angular_z = 0.0;
            break;
        case CartesianAngularVelocity:
            kinova_twist_command.twist.linear_x = 0.0;
            kinova_twist_command.twist.linear_y = 0.0;
            kinova_twist_command.twist.linear_z = 0.0;
        
            kinova_twist_command.twist.angular_x = twist_command_gain.at(3)*(master_twist_command.twist.angular.x);
            kinova_twist_command.twist.angular_y = twist_command_gain.at(4)*(master_twist_command.twist.angular.y);
            kinova_twist_command.twist.angular_z = twist_command_gain.at(5)*(master_twist_command.twist.angular.z);
            break;
        case CartesianLinearVelocityTool:
            kinova_twist_command.twist.linear_x = twist_command_gain.at(0)*(master_twist_command.twist.linear.y); // Omni Base to  Kinova Tool
            kinova_twist_command.twist.linear_y = twist_command_gain.at(1)*(master_twist_command.twist.linear.z);
            kinova_twist_command.twist.linear_z = twist_command_gain.at(2)*(master_twist_command.twist.linear.x);

            kinova_twist_command.twist.angular_x = 0.0;
            kinova_twist_command.twist.angular_y = 0.0;
            kinova_twist_command.twist.angular_z = 0.0;
            break;
        case CartesianAngularVelocityTool:
            kinova_twist_command.twist.linear_x = 0.0;
            kinova_twist_command.twist.linear_y = 0.0;
            kinova_twist_command.twist.linear_z = 0.0;

            kinova_twist_command.twist.angular_x = twist_command_gain.at(3)*(master_twist_command.twist.angular.y); // Omni Base to Kinova Tool
            kinova_twist_command.twist.angular_y = twist_command_gain.at(4)*(master_twist_command.twist.angular.z);
            kinova_twist_command.twist.angular_z = twist_command_gain.at(5)*(master_twist_command.twist.angular.x);
            break;
        default:
            break;
        }

    } else {
        ROS_WARN_STREAM_DELAYED_THROTTLE(5, "Twist message must be received every" << safety_timeout <<
        "seconds or less. Sending twist = 0.0");
        kinova_twist_command.twist.linear_x = 0.0;
        kinova_twist_command.twist.linear_y = 0.0;
        kinova_twist_command.twist.linear_z = 0.0;

        kinova_twist_command.twist.angular_x = 0.0;
        kinova_twist_command.twist.angular_y = 0.0;
        kinova_twist_command.twist.angular_z = 0.0;

    }
    pub_cartesian_velocity.publish(kinova_twist_command);
}

void KinovaRemap::publishJointVelocity() {
    // joint_speed.duration not yet implemeted, so it is used as a "header.seq".
    // Once it is implemented, duration in milliseconds.
    static int duration = 0;
    kinova_joint_speeds.duration = duration++;

    for (int i = 0; i < 7; i++) {
        kinova_joint_speeds.joint_speeds.at(i).joint_identifier = i;
    }

    double time_delta = (ros::Time::now() - msg_time_joint_command).toSec();

    if (time_delta < safety_timeout){
        for (int i=0; i<7; i++) {
            kinova_joint_speeds.joint_speeds.at(i).value = joint_command_gain.at(i)*master_joint_command.velocity.at(i);
        }
        switch (control_mode) {
        case JointVelocityArm:
            kinova_joint_speeds.joint_speeds.at(4).value = 0.0;
            kinova_joint_speeds.joint_speeds.at(5).value = 0.0;
            kinova_joint_speeds.joint_speeds.at(6).value = 0.0;
            break;
        case JointVelocityWrist:
            kinova_joint_speeds.joint_speeds.at(0).value = 0.0;
            kinova_joint_speeds.joint_speeds.at(1).value = 0.0;
            kinova_joint_speeds.joint_speeds.at(2).value = 0.0;
            kinova_joint_speeds.joint_speeds.at(3).value = 0.0;
            break;
        default:
            break;
        }
    } else {
        ROS_WARN_STREAM_DELAYED_THROTTLE(5, "Joint command message must be received every " << safety_timeout <<
        "seconds or less. Sending joint_speeds = 0.0");
        for (int i = 0; i < 7; i++) {
            kinova_joint_speeds.joint_speeds.at(i).value = 0.0;
        }
    }

    pub_joint_velocity.publish(kinova_joint_speeds);
}

void KinovaRemap::publishTopic() {
    switch (control_mode) {
    case JointVelocity:
    case JointVelocityArm:
    case JointVelocityWrist:
        publishJointVelocity();
        break;
    case CartesianVelocity:
    case CartesianLinearVelocity:
    case CartesianAngularVelocity:
    case CartesianLinearVelocityTool:
    case CartesianAngularVelocityTool:
        publishCartesianVelocity();
        break;
    default:
        break;
    }
}

bool KinovaRemap::send_gripper_command(double value) {
    // From kortex_examples/src/full_arm/example_full_arm_movement.cpp
    // Initialize the ServiceClient
    ros::ServiceClient service_client_send_gripper_command = nh->serviceClient<kortex_driver::SendGripperCommand>("/kinova/base/send_gripper_command");
    kortex_driver::SendGripperCommand service_send_gripper_command;

    // Initialize the request
    kortex_driver::Finger finger;
    finger.finger_identifier = 0;
    finger.value = value;
    service_send_gripper_command.request.input.gripper.finger.push_back(finger);
    service_send_gripper_command.request.input.mode = kortex_driver::GripperMode::GRIPPER_POSITION;

    if (service_client_send_gripper_command.call(service_send_gripper_command))  
    {
        //ROS_INFO("The gripper command was sent to the robot.");
    }
    else
    {
        std::string error_string = "Failed to call SendGripperCommand";
        ROS_ERROR("%s", error_string.c_str());
        return false;
    }

    return true;
}

// void KinovaRemap::buttonStateCallback(const omni_msgs::OmniButtonEvent::ConstPtr& msg) {
// 
//     if (msg->grey_button_clicked) {
//         example_send_gripper_command(1.0); // Close gripper
//     } else if (msg->white_button_clicked) {
//         example_send_gripper_command(0.0); // Open gripper
//     }
// }

bool KinovaRemap::action2kinova(int action_identifier) {
    // From kortex_examples/src/full_arm/example_full_arm_movement.cpp
    ros::ServiceClient service_client_read_action = nh->serviceClient<kortex_driver::ReadAction>("/kinova/base/read_action");
    kortex_driver::ReadAction service_read_action;

    // The Kinova Action is used to execute a robot task.
    // For example the HOME Action send  the robot to home . It cannot be deleted and is always ID #2
    service_read_action.request.input.identifier = action_identifier;

    if (!service_client_read_action.call(service_read_action)) {
        std::string error_string = "Failed to call ReadAction";
        ROS_ERROR("%s", error_string.c_str());
        return false;
    }

    // We can now execute the Action that we read 
    ros::ServiceClient service_client_execute_action = nh->serviceClient<kortex_driver::ExecuteAction>("/kinova/base/execute_action");
    kortex_driver::ExecuteAction service_execute_action;

    service_execute_action.request.input = service_read_action.response.output;
    
    if (service_client_execute_action.call(service_execute_action)) {
        ROS_INFO("An action was sent to the robot.");
    } else {
        std::string error_string = "Failed to call ExecuteAction";
        ROS_ERROR("%s", error_string.c_str());
        return false;
    }

    return true;
}

int main(int argc, char **argv){

    ros::init(argc, argv, "kinova_remap");

    KinovaRemap KinovaRemap;
    
    double ros_rate;
    ros::param::param<double>("/kinova/kinova_driver/cyclic_data_publish_rate", ros_rate, 50);
    ros::Rate rate(ros_rate);

    ROS_INFO_STREAM("ROS rate (hz): " << ros_rate);

    ros::Duration(1.0).sleep();

    while (ros::ok())
    {
        ros::spinOnce();

        KinovaRemap.publishTopic();

        rate.sleep();
    }

    return 0;
}
